﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Message_box
{
    class Program
    {
        static void Main(string[] args)
        { 

           Inimene thea = new Inimene("48605100265")
        {
            Isikukood = "48605100265",
            Eesnimi = "Thea",
            Perenimi = "Rehepapp",
        };
            try //PROOVI TEHA SEDA ALUMIST, kui ei saa teha siis seda ei viida täide
            {
                string Nimi = Console.WriteLine("Mis on su eesnimi: ");
                Console.WriteLine("Mis on su isikukood: ");
                int Isikukood = int.Parse(Console.ReadLine());
                {
                    Console.WriteLine($"{Nimi}, sa oled {Vanus}. aastat vana, su sünnipäev on {Synniaeg} ning sa oled {Sugu}.");
                }
            }
            //catch (Exception) //Exceptionite järjekord on tähtis
            //{
            //    Console.WriteLine("Palun ära jäta ühtki välja tühjaks");
            //}
            catch (FormatException)
            {
                Console.WriteLine("Palun sisesta õiged andmed");
            }
            catch (Exception e) //KUI MIDAGI JUHTUB, SIIS TEE SEDA ALUMIST, ERANDOLUKORD.''e'' võid panna siis, kui tahad veateadet saada.
            {
                Console.WriteLine("Miski läks valesti ;)");
                Console.WriteLine(e.Message); //saad teada mis valesti läks
                //throw; kui tahad et sulle öeldakse ka viga. Kui ei taha veateadet, siis seda ei pea panema
            }
            finally
            {
                Console.WriteLine("Aitäh vastamast!");//toimingud mis tehakse sõtumata kas täideti try, või catch
            }
        }

    }
    class Inimene
    {
        public enum Sugu { Naine, Mees }
        public string Nimi;
        string isikukood;
        public string Isikukood //enne igasuguste vanuste ja bla bla arvutamist tuleks kontrollida kas IK on ikka õige, lenght ja kuud ja päevad ja sugu
        {
            get => ik;
            set
            {
                if (value.Length != 11) throw new Exception("Isikukoodi pikkus on vake");//kontrollida kas isikukoodi pikkus on 11
                ik = value;
            }
        }
        public DateTime Synniaeg => new DateTime(Aasta(), int.Parse[]Isikukood.Substring(3, 2)), int.Parse
        public int Vanus //=> (DateTime.Today - Synniaeg).Days*4/1461;
        { get //get funktsioon teeb sellest property
            { return (DateTime.Today - Synniaeg).Days * 4 / 1461; }
        }
        public Sugu Sugu => (Sugu)(this.Isikukood[0] % 2);

        public Inimene(string ik) //kõikidel inimestel PEAB olema Isikukood
        {
            this.Isikukood = ik;
        }
        //int Aasta => ((Isikukood[0] - 1) / 2 - 6) * 100 + int.Parse(Isikukood.Substring(1, 2)); //kahega jagamisel annavad kaks järjestikust arvu ühe tulemuse, nt 4/2 on 2 ja 5/2 on 2
        {
        int aasta = int.Parse(ik.Substring(1, 2));
        switch (ik.Substring(0,1))//võta esimesest märgist üks märk
            {
            case "1": case "2": return aasta + 1000;
            case "3": case "4": return aasta + 1900;
            case "5": case "6": return aasta + 2000;
            }




    }
